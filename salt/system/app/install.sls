include:
  - pkg.before_install
{% if (pillar['postgres'] is defined) and (pillar['postgres'] is not none) %}
  - postgres
{% endif %}
  - percona.percona
  - pyenv.pyenv
  - sentry.sentry
  - php-fpm.php-fpm
  - nginx.nginx
{% if (pillar['java'] is defined) and (pillar['java'] is not none) %}
  - sun-java.opt_dir
  - sun-java
  - sun-java.env
{% endif %}
{% if (pillar['zookeeper'] is defined) and (pillar['zookeeper'] is not none) %}
  - zookeeper
  - zookeeper.config
  - zookeeper.server
{% endif %}
{% if (pillar['atlassian-jira'] is defined) and (pillar['atlassian-jira'] is not none) %}
  - atlassian-jira
{% endif %}
{% if (pillar['node'] is defined) and (pillar['node'] is not none) %}
  - node
{% endif %}
{% if (pillar['elasticsearch'] is defined) and (pillar['elasticsearch'] is not none) %}
  - elasticsearch
{% endif %}
{% if (pillar['mongodb'] is defined) and (pillar['mongodb'] is not none) %}
  - mongodb
{% endif %}
{% if (pillar['redis'] is defined) and (pillar['redis'] is not none) %}
  - redis
{% endif %}
  - pkg.after_install
