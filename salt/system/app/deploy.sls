include:
  - pkg.before_deploy
  - app.php-fpm_apps
  - app.static_apps
  - app.python_apps
  - pkg.after_deploy
