base:

  # All Windows
  'G@kernel:Windows and I@highstated:True':
    - match: compound
    - users.windows

  # All Linux
  'G@kernel:Linux and I@highstated:True':
    - match: compound
    - users.unix
    - ntp.ntp
    - netdata.netdata
    - pkg.pkg
    - test.test

  # All Ubuntu and Debian
  '( G@os:Ubuntu or G@os:Debian ) and I@highstated:True':
    - match: compound
    - vim.vim
    - bash.bash_completions
    - bash.bash_misc
    - ufw_simple.ufw_simple
