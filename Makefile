git_hoock:


docker_build:
	docker build .docker/systemd/ -t systemd
	docker build .docker/master/ -t salt_master
	docker build .docker/base_minion/ -t salt_base_minion
	docker-compose build

docker_down:
	docker-compose down -v --remove-orphans

fix-permission:
	sudo chown -R $(shell whoami):$(shell whoami) .docker/*
	sudo chown -R $(shell whoami):$(shell whoami) *
