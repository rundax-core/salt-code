#!/bin/bash
echo "To: mon@company.com" > /tmp/salt_out.txt
echo "Subject: salt-prod-1.system.com HighState Native" >> /tmp/salt_out.txt
echo "MIME-Version: 1.0" >> /tmp/salt_out.txt
echo "Content-Type: text/html" >> /tmp/salt_out.txt
echo "" >> /tmp/salt_out.txt
( salt --force-color -t 300 'salt-prod-1.system.com' state.highstate --state-output=filter --state-verbose=False exclude=True, 2>&1 | aha --black --line-fix --title "salt-prod-1.system.com HighState Native" | sed -e 's/<pre/<pre style="font-size: 12px"/' | sed -e 's#</span><span#</span\n><span#g' ) >> /tmp/salt_out.txt 2>&1
( salt --force-color -t 300 -C 'I@highstated:True and not salt-prod-1.system.com' state.highstate --state-output=filter --state-verbose=False exclude=True, 2>&1 | aha --black --line-fix --title "salt-prod-1.system.com HighState Native" | sed -e 's/<pre/<pre style="font-size: 12px"/' | sed -e 's#</span><span#</span\n><span#g' ) >> /tmp/salt_out.txt 2>&1
/usr/sbin/sendmail -t < /tmp/salt_out.txt
