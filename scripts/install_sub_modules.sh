#!/bin/bash
git submodule init
git pull --recurse-submodules
git submodule update --recursive --remote
git submodule foreach git pull origin master