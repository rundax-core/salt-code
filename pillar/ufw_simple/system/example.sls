{% from '/srv/pillar/ufw_simple/vars.jinja' import vars with context %}
{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_7_br0 with context %}
{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_8_br0 with context %}
{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_9_br0 with context %}
{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_10_br0 with context %}
{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_11_br0 with context %}

ufw_simple:
  enabled: True
  logging: 'off'
  allow:
    mysql:
      proto: 'tcp'
      from:
        'k8s-master-1.system.com': {{ hetzner_7_br0['k8s-master-1.system.com'] }}
        'k8s-master-2.system.com': {{ hetzner_8_br0['k8s-master-2.system.com'] }}
        'k8s-node-1.system.com': {{ hetzner_9_br0['k8s-node-1.system.com'] }}
        'k8s-node-2.system.com': {{ hetzner_10_br0['k8s-node-2.system.com'] }}
        'k8s-ingress-1.system.com': {{ hetzner_11_br0['k8s-ingress-1.system.com'] }}
      to_port: '3306'
