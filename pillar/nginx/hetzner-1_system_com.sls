nginx:
  enabled: True
  reload: True
  configs: 'nginx/app_proxy'
  www_dir: '/var/www'
  forward:
    bank-prod-1:
      enabled: True
      server_name: 'site-prod-1.system.com'
      proxy_to: '10.0.10.2'
#      ssl:
#        acme: True