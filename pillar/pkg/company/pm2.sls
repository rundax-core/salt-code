node:
  version: 8.11.3-1nodesource1
  install_from_ppa: True
  ppa:
    repository_url: https://deb.nodesource.com/node_8.x
  pkg_manager: 'yarn'
  pkgs_global:
    - https://github.com/wirwolf/pm2.git#master
    - elasticdump

pkg:
  pm2_install:
    when: 'PKG_BEFORE_DEPLOY'
    states:
      - module.run:
          1:
            - name: state.sls
            - mods: node

      - cmd.run:
          1:
            - name: 'npm install -g yarn'
          2:
            - name: 'yarn global add https://github.com/wirwolf/pm2.git#master'
