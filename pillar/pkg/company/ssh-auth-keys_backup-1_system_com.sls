pkg:
  ssh-auth-keys_1:
    when: 'PKG_PKG'
    states:
      - ssh_auth.present:
          1:
            - user: 'root'
            - names:
              - 'ssh-rsa '
      #- ssh_auth.absent:
      #    1:
      #      - user: 'root'
      #      - names:
      #        - 'xxx'
