pkg:
  nodejs:
    when: 'PKG_BEFORE_DEPLOY'
    states:
      - pkgrepo.managed:
          1:
            - humanname: NodeJS Repository
            - name: deb [arch=amd64] https://deb.nodesource.com/node_8.x {{ grains['oscodename'] }} main
            - file: /etc/apt/sources.list.d/nodesource.list
            - key_url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
          2:
            - humanname: Yarn
            - name: deb https://dl.yarnpkg.com/debian/ stable main
            - file: /etc/apt/sources.list.d/yarn.list
            - key_url: https://dl.yarnpkg.com/debian/pubkey.gpg
      - pkg.installed:
          1:
            - refresh: True
            - pkgs:
              - nodejs
              - yarn
