{% from '/srv/pillar/ip/hetzner.jinja' import hetzner_1_br0 with context %}

softether:
  # Do not use tag here, it cannot be rev parsed then
  version: 'c23142a8ffc35088047060989fc3d76c5b4e00af'
  vpnserver:
    install: True
    # Main management password
    password: '-------------'
    cmds:
      - 'IPsecEnable /L2TP:yes /L2TPRAW:no /ETHERIP:yes /PSK:PskKsp17 /DEFAULTHUB:system_no_default_gw'
      - 'OpenVpnMakeConfig "\/opt/softether/vpnserver/openvpnconfig.zip"'
    hubs:
      {%-
      set servers = [
        hetzner_1_br0['hetzner-1.system.com']
      ]
      %}
      DEFAULT:
        delete: True
      system:
        delete: True
      system_no_default_gw:
	{%- set network = '192.168.30' %}
        password: '************************'
        cmds:
          - 'SecureNatEnable'
          - 'SetEnumDeny'
          - 'SecureNatHostSet /MAC:none /IP:{{ network }}.1 /MASK:255.255.255.0'
          - 'DhcpSet /START:{{ network }}.10 /END:{{ network }}.200 /MASK:255.255.255.0 /EXPIRE:7200 /GW:none /DNS:none /DNS2:none /DOMAIN:system.com /LOG:yes /PUSHROUTE:"{% for server_ip in servers %}{{ server_ip }}/255.255.255.255/{{ network }}.1{% if not loop.last > 1 %},{% endif %}{% endfor %}"'
        users:
          andru:
            password: '*************'
            realname: 'Andru'