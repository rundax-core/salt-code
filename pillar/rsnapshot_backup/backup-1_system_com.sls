rsnapshot_backup:
  sources:
    backup-1.system.com:
      - type: FS_RSYNC_SSH
        data:
          - UBUNTU
        checks:
          - type: .backup
        backups:
          - host: backup-1.system.com
            path: /var/backups/backup-1.system.com
