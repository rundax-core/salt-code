# ACHTUNG!!!
# Do not edit /srv/pillar/top.sls directly.
# It is being compiled by concatenating all files within /srv/pillar/top_sls directory.
# Compilation is triggered by post-merge git hook.

base:

  # Common pillars by OS 

  'os:Ubuntu':
    - match: grain
    - vim.vim
    - users.base

  'os:Debian':
    - match: grain
    - vim.vim
    - users.base

