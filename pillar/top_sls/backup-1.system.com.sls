  'backup-1.system.com':
    - inventory.backup-1_system_com
    - pkg.common
    - sysadmws-utils.v0_latest
    - sysadmws-utils.v1_latest
    - notify_devilry.system
    - users.groups.devops
    - ufw_simple.standard
    - pkg.tz.Etc_UTC
    - pkg.locale.en_US_UTF-8
    - netdata.1d
    - netdata.common
    - rsnapshot_backup.backup_server
    - rsnapshot_backup.backup-1_system_com
    - rsnapshot_backup.legacy

