users:
  andru:
    fullname: Andru
    empty_password: False
    enforce_password: True
    password: '!'
    home: /home/andru
    user_dir_mode: 755
    createhome: True
    shell: /bin/bash
    prime_group:
      name: andru
    ssh_auth_file:
      - ssh-rsa *****************************************************************************
    sudouser: True
    sudo_rules:
      - ALL=(ALL) NOPASSWD:ALL
    sudo_defaults:
      - 'env_keep+=SSH_AUTH_SOCK'
      - 'env_keep+=EDITOR'
    groups:
      - sudo
      - devops
    user_files:
      enabled: True
      source: users/andru
