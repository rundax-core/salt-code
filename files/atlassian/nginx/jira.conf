server {
  listen 80;
  server_name jira.system.com;
  location / {
    return 301 https://jira.system.com$request_uri;
  }
}

server {
  listen 443 ssl http2;
  server_name jira.system.com;

  include snippets/ssl-params.conf;

  ssl_certificate /opt/acme/cert/jira_fullchain.cer;
  ssl_certificate_key /opt/acme/cert/jira_key.key;

  access_log /var/log/nginx/jira.access.log;
  error_log /var/log/nginx/jira.error.log;

  client_max_body_size 200M;
  client_body_buffer_size 128k;

  location / {
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://{{ jira_server_ip }}:8080;
  }
}
