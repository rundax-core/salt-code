server {
  listen 80;
  server_name repo.system.com;
  location / {
    return 301 https://repo.system.com$request_uri;
  }
}

upstream nexus {
  server {{ repo_server_ip }}:8081;
}

upstream registry {
  server {{ repo_server_ip }}:5000;
}

server {
  listen 443 ssl http2;
  server_name repo.system.com;

  include snippets/ssl-params.conf;

  ssl_certificate /opt/acme/cert/repo_fullchain.cer;
  ssl_certificate_key /opt/acme/cert/repo_key.key;

  client_max_body_size 1G;

  access_log /var/log/nginx/repo.access.log;
  error_log /var/log/nginx/repo.error.log;

  location / {
    # redirect to docker registry
    if ($http_user_agent ~ docker ) {
      proxy_pass http://registry;
    }
    proxy_pass http://nexus;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto "https";
  }
}