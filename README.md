build docker images
```
make docker_build
```

up ecosystem

```
docker-compose up -d
```

wait 5 seconds

Enter master container
```
docker-compose exec master bash
```

check connected minions
```
salt-key
salt "*" test.ping
```
